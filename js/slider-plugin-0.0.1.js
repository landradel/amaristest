
var AmarisSlider = function (){

	// Define plugin implementation
	var slider = $( "#banner-container" );
	 	
	var numberOfSlides = 0;

	// Slide interval
	var sliderInit = function(){
		
		// Fin banner container
		var $bannerContainer = $("#banner-container");

		// Change to next element
		$bannerContainer.find(".slide.active").each(function(index, element){
			
			// Cast to jQuery object
			var $element = $(element);
			
			// Check if the element have the active class 
			if($element.hasClass("fadeInRight")){
				
				// Hide element
				$(this).removeClass("fadeInRight").addClass("fadeOutLeft").removeClass("active");

				// Add class to next element
				var nextClassElement = "."+$element.attr("data-next-slide");
				
				// Show next element
				$(nextClassElement).addClass("active").addClass("fadeInRight").removeClass("fadeOutLeft");

				// Change active in dots slider
				$(".dot-anchor").removeClass("active");

				// Get dot target
				var dotTarget = nextClassElement + "-dot";

				// Add dot target active class
				$(dotTarget).addClass("active");
				
			}
		});
	};

	// Do slide change interval
	var slideChangeIntervalID = setInterval(sliderInit, 8000);

	// Hide all slide but the first one
	slider.find(".slide").each(function(index, element){
		
		// Cast to jQuery object
		var $element = $(element);
		
		// Validate index
		if(index == 0){
			// Add active class
			$element.addClass("fadeInRight");
		}

		// Add class to attach click event
		$element.addClass("sld-" + index);

		// Add animate.css default animated class
		$element.addClass("animated");

		// Add next element
		if(slider.find(".slide").length == (index + 1)){
			// Add data attribute to target the next slide
			$element.attr("data-next-slide", "sld-0");
		}
		else{
			// Add data attribute to target the next slide
			$element.attr("data-next-slide", "sld-" + (index + 1));
		}
		
		// Count ++
		numberOfSlides++;

	});
    
    // Build dots structure
	var dotsContent = '<div class="banner-dots-container">';
	for (var i = 0; i < numberOfSlides; i++) {
		if(i == 0){
			dotsContent = dotsContent + '<a data-slider="'+ i +'" class="dot-anchor sld-'+ i +'-dot active"><span class="banner-dot"></span></a>';
		}
		else{
			dotsContent = dotsContent + '<a data-slider="'+ i +'" class="dot-anchor sld-'+ i +'-dot"><span class="banner-dot"></span></a>';
		}
	}
	dotsContent = dotsContent + '</div>';

	// Append dots content
	slider.append(dotsContent);

    // Set active anchor
    slider.find( ".dot-anchor" ).each(function(index, element) {
       // Cast to jQuery object
		var $element = $(element);
		// Validate index
		if(index == 0){
			// Add active class
			$(element).addClass("active");
		}
    });

		// Add click event
    slider.find( ".dot-anchor" ).on("click", function(event){
    	// Clear actual interval
		clearInterval(slideChangeIntervalID);

    	// Cast to jQuery object
    	var $element = $(this);

    	// Get target to active
		var targetToActivate = $element.attr("data-slider");

		// Remove all dot active class
		$(".dot-anchor").removeClass("active");

		// Add this element class
		$(this).addClass("active");

		// Hide active slide
		$(".slide.active").removeClass("fadeInRight").addClass("fadeOutLeft").removeClass("active");

		var targetOfNewActiveSlide = ".sld-" + targetToActivate;
		
		// Show active slide
		$(targetOfNewActiveSlide).addClass("fadeInRight").addClass("active").removeClass("fadeOutLeft");

		// Create interval again
		slideChangeIntervalID = setInterval(sliderInit, 8000);

		
    });
}();
