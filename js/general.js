// App specification
var utilities = utilities || {
	ChangeSlider: function(){
		// Get banner jQuery element
		var $bannerContainer = $("#banner-container");
		// Find window element
		$bannerContainer.find(".window-element").css("display","block");
		// Show window element
		$bannerContainer.find(".window-element").fadeTo( 1000, 1, function(){
			// Toggle big and smal slider classes
			$bannerContainer.toggleClass("small-slider").toggleClass("big-slider");

			// Hide window element
			$bannerContainer.find(".window-element").fadeTo( 1000, 0, function(){
				// Add display none to window element
				$(this).css("display","none");

				// Show nav section
				utilities.ShowNavLinks();

				// Show nav contents
				utilities.ShowNavContents();
			});
		});
	},
	ShowLeftMenu: function(){
		// Hide center menu options
    	$(".centerMenuOptions").fadeTo( 1000, 0, function(){
    		// Hide center menu options complitely
    		$(this).css("display","none");
			// Show left menu options
			$(".leftMenuOptions").fadeTo( 1000, 1, function(){});
    	});
	},
	ShowMainMenu: function(){
		// Hide left menu options
    	$(".leftMenuOptions").fadeTo( 1000, 0, function(){

			// Show center menu options
    		$(".centerMenuOptions").fadeTo( 1000, 1, function(){

			});
    	});
	},
	ShowNavLinks: function(){
		// Hide left menu options
    	$(".nav-section").css("display","block").fadeTo( 1000, 1, function(){});	
	},
	ShowNavContents: function(){
		// Hide left menu options
    	$(".nav-contents").css("display","block").fadeTo( 1000, 1, function(){});	
	},
}