var app = angular.module('amarisApp', []);
app.controller('HomeController', function($scope) {
    // Simulate main array menu
    $scope.mainMenus = [
    	{
    		Title:"Menu 1",
    		ImgSrc: "imgs/ln_image1.jpg",
    		SubMenuIconClass:"red-dot",
    		SpecialClass: "",
    		SubMenus:[
    			{
    				ID: 1,
    				Title:"Global Analytics",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"Brand Analysis",
		    			},
		    			{
		    				ID: 13,
		    				Title:"Income Statement Analysis",
		    			},
		    			{
		    				ID: 14,
		    				Title:"FTEs/Personnel Costs",
		    			},
		    			{
		    				ID: 15,
		    				Title:"Management Cash Flow"
		    			},
		    			{
		    				ID: 16,
		    				Title:"LO History"
		    			},
		    			{
		    				ID: 17,
		    				Title:"S&OP Analysis"
		    			},
		    			{
		    				ID: 18,
		    				Title:"GenMed MBR Tool"
		    			},
		    			{
		    				ID: 19,
		    				Title:"Onco MBR Tool"
		    			},
		    			{
		    				ID: 20,
		    				Title:"Ad-hoc Analysis"
		    			}
		    		]
		    	},
		    	{
		    		ID: 2,
    				Title:"Local Analytics",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"Local Analysis",
		    			},
		    			{
		    				ID: 13,
		    				Title:"Local Analysis",

		    			},
		    			{
		    				ID: 14,
		    				Title:"Local Analysis",
		    			},
		    			{
		    				ID: 15,
		    				Title:"Local Analysis"
		    			},
		    			{
		    				ID: 16,
		    				Title:"Local Analysis"
		    			},
		    			{
		    				ID: 17,
		    				Title:"Local Analysis"
		    			},
		    			{
		    				ID: 18,
		    				Title:"Local Analysis"
		    			},
		    			{
		    				ID: 19,
		    				Title:"Onco MBR Tool"
		    			},
		    			{
		    				ID: 20,
		    				Title:"Local Analysis"
		    			}
		    		]
		    	}
	    	]
    	},
    	{
    		Title:"Menu 2",
    		ImgSrc: "imgs/ln_image2.jpg",
    		SubMenuIconClass:"yellow-dot",
    		SpecialClass: "",
    		SubMenus:[
    			{
    				ID: 3,
    				Title:"MBR Decks",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"MBR Decks",
		    			},
		    			{
		    				ID: 13,
		    				Title:"MBR Decks",

		    			},
		    			{
		    				ID: 14,
		    				Title:"MBR Decks",
		    			},
		    			{
		    				ID: 15,
		    				Title:"MBR Decks"
		    			},
		    			{
		    				ID: 16,
		    				Title:"MBR Decks"
		    			},
		    			{
		    				ID: 17,
		    				Title:"MBR Decks"
		    			},
		    			{
		    				ID: 18,
		    				Title:"MBR Decks"
		    			},
		    			{
		    				ID: 19,
		    				Title:"MBR Decks"
		    			},
		    			{
		    				ID: 20,
		    				Title:"MBR Decks"
		    			}
		    		]
    			},
    			{
    				ID: 4,
    				Title: "IMR Publications",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"IMR Publications",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"IMR Publications",
		    			},
		    			{
		    				ID: 13,
		    				Title:"IMR Publications",

		    			},
		    			{
		    				ID: 14,
		    				Title:"IMR Publications",
		    			},
		    			{
		    				ID: 15,
		    				Title:"IMR Publications"
		    			},
		    			{
		    				ID: 16,
		    				Title:"IMR Publications"
		    			},
		    			{
		    				ID: 17,
		    				Title:"IMR Publications"
		    			},
		    			{
		    				ID: 18,
		    				Title:"IMR Publications"
		    			},
		    			{
		    				ID: 19,
		    				Title:"IMR Publications"
		    			},
		    			{
		    				ID: 20,
		    				Title:"IMR Publications"
		    			}
		    		]
    			}
    		]
    	},
    	{
    		Title:"Menu 3",
    		ImgSrc: "imgs/ln_image3.jpg",
    		SubMenuIconClass:"orange-dot",
    		SpecialClass: "special-size",
    		SubMenus:[
    			{
    				ID: 5,
    				Title:"Tableau",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"Tableau",
		    			},
		    			{
		    				ID: 13,
		    				Title:"Tableau",

		    			},
		    			{
		    				ID: 14,
		    				Title:"Tableau",
		    			},
		    			{
		    				ID: 15,
		    				Title:"Tableau"
		    			},
		    			{
		    				ID: 16,
		    				Title:"Tableau"
		    			},
		    			{
		    				ID: 17,
		    				Title:"Tableau"
		    			},
		    			{
		    				ID: 18,
		    				Title:"Tableau"
		    			},
		    			{
		    				ID: 19,
		    				Title:"Tableau"
		    			},
		    			{
		    				ID: 20,
		    				Title:"Tableau"
		    			}
		    		]
    			}
    		]
    	},
    	{
    		Title:"Menu 4",
    		ImgSrc: "imgs/ln_image4.jpg",
    		SubMenuIconClass:"maroon-dot",
    		SpecialClass: "",
    		SubMenus:[
    			{
    				ID: 6,
    				Title:"Global (TM1)",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"Global (TM1)",
		    			},
		    			{
		    				ID: 13,
		    				Title:"Global (TM1)",

		    			},
		    			{
		    				ID: 14,
		    				Title:"Global (TM1)",
		    			},
		    			{
		    				ID: 15,
		    				Title:"Global (TM1)"
		    			},
		    			{
		    				ID: 16,
		    				Title:"Global (TM1)"
		    			},
		    			{
		    				ID: 17,
		    				Title:"Global (TM1)"
		    			},
		    			{
		    				ID: 18,
		    				Title:"Global (TM1)"
		    			},
		    			{
		    				ID: 19,
		    				Title:"Global (TM1)"
		    			},
		    			{
		    				ID: 20,
		    				Title:"Global (TM1)"
		    			}
		    		]
    			},
    			{
    				ID: 7,
    				Title:"Local (SAP BPC)",
    				NavSections:[
		    			{
		    				ID: 11,
		    				Title:"Sales Analysis",
		    				Activo: "active",
		    				SectionContent:[
		    					{
		    						ID: 20,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_1.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 21,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_2.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					},
		    					{
		    						ID: 22,
		    						Title: "Sales",
		    						ImgSrc:"imgs/box_3.jpg",
		    						SubMenuIconClass:"red-dot",
		    						Description: "Sales Performance by Country"
		    					}
		    				]
		    			},
		    			{
		    				ID: 12,
		    				Title:"Local (SAP BPC)",
		    			},
		    			{
		    				ID: 13,
		    				Title:"Local (SAP BPC)",

		    			},
		    			{
		    				ID: 14,
		    				Title:"Local (SAP BPC)",
		    			},
		    			{
		    				ID: 15,
		    				Title:"Local (SAP BPC)"
		    			},
		    			{
		    				ID: 16,
		    				Title:"Local (SAP BPC)"
		    			},
		    			{
		    				ID: 17,
		    				Title:"Local (SAP BPC)"
		    			},
		    			{
		    				ID: 18,
		    				Title:"Local (SAP BPC)"
		    			},
		    			{
		    				ID: 19,
		    				Title:"Local (SAP BPC)"
		    			},
		    			{
		    				ID: 20,
		    				Title:"Local (SAP BPC)"
		    			}
		    		]
    			}
    		]
    	}
    ];

    // Make it empty by default
    $scope.navLink = {
    	SubMenuIconClass:"",
    	items:[]
    };

    $scope.navContent = {
    	contents:[],
    	links:[
    		{
    			Title:"Sales Performance",
    			items:[
	    			{
	    				Title:"Sales Performance by Country"
	    			},
	    			{
	    				Title:"Sales Performance by Brand"
	    			},
	    			{
	    				Title:"Sales Waterfall by Brand"
	    			},
	    			{
	    				Title:"Sales Waterfall by Country"
	    			}
    			]
    		},
    		{
    			Title:"Other Sales Analysis Tools",
    			items:[
	    			{
	    				Title:"Sales By Month by Product"
	    			},
	    			{
	    				Title:"Sales By Month by Country"
	    			},
	    			{
	    				Title:"Sales By Month by Couuntry by SKU"
	    			},
	    			{
	    				Title:"Sales & Quantities of Product"
	    			},
	    			{
	    				Title:"Sales by History Analysis"
	    			}
    			]
    		}
    	]
    }

    // Show content
    $scope.ShowMainContent = function(menu, submenu){
    	// Change slider structure
    	utilities.ChangeSlider();

    	// Get class of item selected
		var itemSelected = ".submenu-"+submenu.ID;

		// Show left menu options
		$(".leftMenuOptions").find(itemSelected).addClass("active");

		// Show left menu
    	utilities.ShowLeftMenu();
		
    	// Set items of nav section
		$scope.navLink.SubMenuIconClass = menu.SubMenuIconClass;
		$scope.navLink.items = submenu.NavSections;

		// Set content of navContent
		$scope.navContent.contents = submenu.NavSections[0].SectionContent;
    	
    };

    // Show content
    $scope.ChangeContent = function(menu, submenu){
    	
    	// Remove all submenus active classes
    	$(".submenus").removeClass("active");

		// Get class of item selected
		var itemSelected = ".submenu-"+submenu.ID;

    	// Show left menu options
		$(".leftMenuOptions").find(itemSelected).addClass("active");
		
		// Set items of nav section
		$scope.navLink.SubMenuIconClass = menu.SubMenuIconClass;
		$scope.navLink.items = submenu.NavSections;

		// Set content of navContent
		$scope.navContent.contents = submenu.NavSections[0].SectionContent;

		utilities.ShowNavLinks();
    };

    // NavChange event
    $scope.NavChange = function(navItem){
    	// Remove all active classes
    	$(".nav-item").removeClass("active");

    	// Add active class
    	$(".nav-item-"+navItem.ID).addClass("active");
    };
});